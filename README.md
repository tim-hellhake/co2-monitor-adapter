# CO₂ Monitor Adapter

Mozilla IoT Adapter for the TFA CO2 Monitor AIRCO2NTROL MINI.

## USB permissions on Linux

Create the file `/etc/udev/rules.d/98-co2mon.rules` with the following contents:
```
SUBSYSTEM=="usb", ATTRS{idVendor}=="04d9", ATTRS{idProduct}=="a052", GROUP="plugdev", MODE="0666"
KERNEL=="hidraw*", ATTRS{idVendor}=="04d9", ATTRS{idProduct}=="a052", GROUP="plugdev", MODE="0666"
```

Run `groups` to ensure the current user is in the group `plugdev`. Otherwise, you may want to add the user to the group or give the permission above to a different group.

Now run `sudo udevadm control --reload-rules && udevadm trigger` and re-plug the USB cable of the CO₂ Monitor.

If you know an automatic way to do this when installing the addon, please contact me or open a Merge Request.
