/**
 * serial-adapter.js - OnOff adapter implemented as a plugin.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.*
 */

'use strict';

const {
  Adapter,
  Device,
  Property,
} = require('gateway-addon');

const Co2Monitor = require('co2-monitor');

class Co2MonitorProperty extends Property {
  constructor(device, name, propertyDescription) {
    super(device, name, propertyDescription);
    this.setCachedValue(propertyDescription.value);
    this.device.notifyPropertyChanged(this);
  }

  /**
   * Set the value of the property.
   *
   * @param {*} value The new value to set
   * @returns a promise which resolves to the updated value.
   *
   * @note it is possible that the updated value doesn't match
   * the value passed in.
   */
  setValue(value) {
    return new Promise((resolve, reject) => {
      super.setValue(value).then((updatedValue) => {
        resolve(updatedValue);
        this.device.notifyPropertyChanged(this);
      }).catch((err) => {
        reject(err);
      });
    });
  }
}

class Co2MonitorDevice extends Device {
  constructor(adapter, id, deviceDescription) {
    super(adapter, id);
    this.name = 'CO₂ Monitor';
    // this.type = deviceDescription.type;
    this['@schema'] = 'https://iot.mozilla.org/schemas/';
    this['@type'] = ['TemperatureSensor'];
    this.description = 'Carbon dioxide monitor';
    for (const propertyName in deviceDescription.properties) {
      const propertyDescription = deviceDescription.properties[propertyName];
      const property = new Co2MonitorProperty(
        this,
        propertyName,
        propertyDescription
      );
      this.properties.set(propertyName, property);
    }
  }
}

class Co2MonitorAdapter extends Adapter {
  constructor(addonManager, manifest) {
    super(addonManager, 'Co2MonitorAdapter', manifest.name);
    addonManager.addAdapter(this);


    if (!this.devices.co2monitor) {
      const device = new Co2MonitorDevice(this, 'co2monitor', {
        properties: {
          carbonDioxideConcentration: {
            '@type': 'CarbonDioxideConcentration',
            label: 'CO₂ (ppm)',
            name: 'carbonDioxideConcentration',
            readOnly: true,
            type: 'integer',
            unit: 'parts-per million',
            value: 0,
          },
          temperature: {
            '@type': 'TemperatureProperty',
            label: 'Temperature',
            multipleOf: 0.001,
            name: 'temperature',
            readOnly: true,
            type: 'number',
            unit: 'degree celsius',
            value: 0,
          },
        },
      });

      const co2Monitor = new Co2Monitor();

      co2Monitor.on('connected', () => {
        co2Monitor.startTransfer();
      });

      co2Monitor.on('error', (error) => {
        console.error(error);
      });

      co2Monitor.on('co2', (co2) => {
        device.properties.get('carbonDioxideConcentration').setCachedValueAndNotify(co2);
      });

      co2Monitor.on('temp', (tempString) => {
        const temp = Number.parseFloat(tempString);
        if (!Number.isNaN(temp)) {
          device.properties.get('temperature').setCachedValueAndNotify(temp);
        }
      });

      co2Monitor.connect();
      this.handleDeviceAdded(device);
    }
  }

  /**
   * Example process to add a new device to the adapter.
   *
   * The important part is to call: `this.handleDeviceAdded(device)`
   *
   * @param {String} deviceId ID of the device to add.
   * @param {String} deviceDescription Description of the device to add.
   * @return {Promise} which resolves to the device added.
   */
  addDevice(deviceId, deviceDescription) {
    return new Promise((resolve, reject) => {
      if (deviceId in this.devices) {
        reject(`Device: ${deviceId} already exists.`);
      } else {
        const device = new Co2MonitorDevice(this, deviceId, deviceDescription);
        this.handleDeviceAdded(device);
        resolve(device);
      }
    });
  }

  /**
   * Example process to remove a device from the adapter.
   *
   * The important part is to call: `this.handleDeviceRemoved(device)`
   *
   * @param {String} deviceId ID of the device to remove.
   * @return {Promise} which resolves to the device removed.
   */
  removeDevice(deviceId) {
    return new Promise((resolve, reject) => {
      const device = this.devices[deviceId];
      if (device) {
        this.handleDeviceRemoved(device);
        resolve(device);
      } else {
        reject(`Device: ${deviceId} not found.`);
      }
    });
  }

  /**
   * Start the pairing/discovery process.
   *
   * @param {Number} timeoutSeconds Number of seconds to run before timeout
   */
  startPairing(_timeoutSeconds) {
    console.log('Co2MonitorAdapter:', this.name,
                'id', this.id, 'pairing started');
  }

  /**
   * Cancel the pairing/discovery process.
   */
  cancelPairing() {
    console.log('Co2MonitorAdapter:', this.name, 'id', this.id,
                'pairing cancelled');
  }

  /**
   * Unpair the provided the device from the adapter.
   *
   * @param {Object} device Device to unpair with
   */
  removeThing(device) {
    console.log('Co2MonitorAdapter:', this.name, 'id', this.id,
                'removeThing(', device.id, ') started');

    this.removeDevice(device.id).then(() => {
      console.log('Co2MonitorAdapter: device:', device.id, 'was unpaired.');
    }).catch((err) => {
      console.error('Co2MonitorAdapter: unpairing', device.id, 'failed');
      console.error(err);
    });
  }

  /**
   * Cancel unpairing process.
   *
   * @param {Object} device Device that is currently being paired
   */
  cancelRemoveThing(device) {
    console.log('Co2MonitorAdapter:', this.name, 'id', this.id,
                'cancelRemoveThing(', device.id, ')');
  }
}

module.exports = Co2MonitorAdapter;
